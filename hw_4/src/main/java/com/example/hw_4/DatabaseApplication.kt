package com.example.hw_4

import android.app.Application

class DatabaseApplication : Application() {
    override fun onCreate() {
        super.onCreate()
        UserRepository.initialize(this)
    }
}