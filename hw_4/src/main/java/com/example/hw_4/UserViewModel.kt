package com.example.hw_4

import android.net.Uri
import android.service.autofill.UserData
import androidx.lifecycle.*
import java.lang.System.currentTimeMillis
import java.util.*

class UserViewModel : ViewModel() {
    private var userRepository = UserRepository.get()
    private val _userViewList = MutableLiveData<List<UserView>>()
    var userViewList: LiveData<List<UserView>> = _userViewList
    private val _userDatabaseList = MutableLiveData<List<UserDatabase>>()
    var userDatabaseList: LiveData<List<UserDatabase>> = _userDatabaseList

    init {
        userDatabaseList = liveData {       // не блокує основний потік
            val data = userRepository.getUsers()
            // delay(5000)
            emitSource(data) // як live data узнала шо відбулись зміни якщо в репозиторію затримка 5 секунд
        }
        userViewList = Transformations.switchMap(userDatabaseList) {list -> convertData(list)}

    }

    private fun convertData(data: List<UserDatabase>): LiveData<List<UserView>> {
        val _userList = MutableLiveData<List<UserView>>()
        val userList: LiveData<List<UserView>> = _userList
        val result = mutableListOf<UserView>()

        for (user in data) {
            result.add(
                UserView(
                    user.name, getImageUri(user.imageName), getVisitTime(user.lastVisit),
                    user.hobby, user.email, user.status, user.id
                )
            )
        }
        _userList.postValue(result)

        return userList
    }

    private fun getImageUri(name : String) : Uri {
        return Uri.parse("android.resource://" + BuildConfig.APPLICATION_ID + "/drawable/" + name)
    }

    private fun getVisitTime(milliseconds : Long) : String {
        val lastVisitInMinutes = (Calendar.getInstance().timeInMillis - milliseconds)/60000
        val result : String = when (lastVisitInMinutes) {
            in 0..5 -> "5 minutes"
            in 5..60 -> "hour"
            in 60..3600 -> "day"
            in 3600..86400 -> "month"
            else -> "year"
        }

        return "Last visit during $result"
    }


    fun updateUser(name: String, email: String, hobby: String, status: String, imageName: String, id: UUID) {
        val updatedUser = UserDatabase(name, imageName, currentTimeMillis(), hobby, email, status, id)

        if (!updatedUser.equals(getUser(id))) {userRepository.updateUser(updatedUser)}
    }

    private fun getUser(id : UUID) : UserView {
        return userViewList.value?.find { it.id == id }!!
    }

//    fun addUser(user: UserDatabase) {
//        userRepository.addUser(user)
//    }
}