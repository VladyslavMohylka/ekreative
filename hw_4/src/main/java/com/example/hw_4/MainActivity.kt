package com.example.hw_4

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import com.example.hw_4.databinding.ActivityMainBinding
import de.hdodenhof.circleimageview.CircleImageView
import java.util.*

class MainActivity : AppCompatActivity() {
    private val userViewModel : UserViewModel by lazy { ViewModelProvider(this).get(UserViewModel::class.java)}
    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        userViewModel.userViewList.observe(this, {userList -> updateUserListView(userList)})
    }

    private fun updateUserListView(userList: List<UserView>) {
        for (user in userList) {
            addUserView(user)
        }
    }

    private fun addUserView(user: UserView) {
        val containerView : View = LayoutInflater.from(this).inflate(R.layout.user_list_view, null)
        containerView.tag = user.id
        val imageView : CircleImageView =  containerView.findViewById(R.id.image)
        imageView.setImageURI(user.imageName)
        val textViewName : TextView =  containerView.findViewById(R.id.name)
        textViewName.text = user.name
        val textViewVisitTime : TextView =  containerView.findViewById(R.id.visit_time)
        textViewVisitTime.text = user.lastVisit

        containerView.setOnClickListener {
            val intent = Intent(this@MainActivity, UserActivity::class.java)
            intent.putExtra("value", containerView.tag as UUID)
            startActivity(intent)
        }
        binding.usersListContainer.addView(containerView, binding.usersListContainer.childCount)
    }
}

