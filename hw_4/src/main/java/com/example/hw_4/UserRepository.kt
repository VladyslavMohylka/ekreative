package com.example.hw_4

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.sqlite.db.SupportSQLiteDatabase
import com.example.hw_4.database.UserDatabase
import kotlinx.coroutines.*
import java.lang.IllegalStateException
import java.util.*
import java.util.concurrent.Executors
import kotlin.random.Random
import kotlin.random.nextLong



private const val DATABASE_NAME = "user_database"

class UserRepository private constructor(context: Context) {
    private val testData = listOf(
        UserDatabase("John", "id_1", getRandomTime(), "sport", "www.john1.com", "sleep", UUID.randomUUID()),
        UserDatabase("Nikolas", "id_3", getRandomTime(), "sport, travel", "www.john3.com", "smile", UUID.randomUUID()),
        UserDatabase("Peter", "id_4", getRandomTime(), "sport, films, travel", "www.john4.com", "other", UUID.randomUUID()),
        UserDatabase("Albert", "id_5", getRandomTime(), "sport, cars", "www.john5.com", "sleep", UUID.randomUUID()),
        UserDatabase("Rick", "id_6", getRandomTime(), "sport, chess", "www.john6.com", "sleep", UUID.randomUUID()),
        UserDatabase("Donald", "id_1", getRandomTime(), "sport, chess", "www.john7.com", "sleep", UUID.randomUUID()),
        UserDatabase("Bred", "id_4", getRandomTime(), "sport, chess", "www.john8.com", "sleep", UUID.randomUUID())
    )
    private val database: UserDatabase = Room.databaseBuilder(
        context.applicationContext,
        UserDatabase::class.java,
        DATABASE_NAME
    ).addCallback(object : RoomDatabase.Callback() {
        override fun onCreate(db: SupportSQLiteDatabase) { //callback ф-ія може взагалі на викликаться якщо : Вам необходимо зарегистрировать хотя бы одного наблюдателя для любого из методов dao, который возвращает LiveData. Когда наблюдатель будет зарегистрирован, будет вызвана ваша roomCallback onCreate.
            super.onCreate(db)
            //CoroutineScope(Dispatchers.Default).launch(Dispatchers.IO) {
            CoroutineScope(Dispatchers.Default).launch {
                // delay(5000)
                get().userDao.addStartUsers(testData)
            }
        }
    }).build()
    private val userDao = database.userDao()
    private val executor = Executors.newSingleThreadExecutor()

    suspend fun updateItems() = withContext(Dispatchers.IO + NonCancellable) {
        get().userDao.addStartUsers(testData)
    }

    fun getUser(id: UUID): LiveData<com.example.hw_4.UserDatabase?> = userDao.getUser(id)

    fun getUsers(): LiveData<List<com.example.hw_4.UserDatabase>> = userDao.getUsers()

    fun updateUser(user: com.example.hw_4.UserDatabase) {
        executor.execute {
            userDao.updateUser(user)
        }
    }

    fun addUser(user: com.example.hw_4.UserDatabase) {
        executor.execute {
            userDao.addUser(user)
        }
    }

    private fun getRandomTime(): Long {
        val calendar = Calendar.getInstance()
        calendar.set(2021, 10, 23, 0, 0, 0)

        return Random.nextLong(calendar.timeInMillis..Calendar.getInstance().timeInMillis)
    }

    companion object {
        private var INSTANCE: UserRepository? = null

        fun initialize(context: Context) {
            if (INSTANCE == null) {
                INSTANCE = UserRepository(context)
            }
        }

        fun get(): UserRepository {
            return INSTANCE ?: throw IllegalStateException("UserRepository must be  initialized")
        }
    }
}
