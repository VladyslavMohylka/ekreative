package com.example.hw_4

import android.app.Activity
import android.content.Context
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.view.inputmethod.InputMethodManager
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.NavUtils
import androidx.lifecycle.ViewModelProvider
import com.example.hw_4.databinding.ActivityUserBinding
import java.util.*
// в ondestroy добавить updateUser
class UserActivity : AppCompatActivity() {
    private val userViewModel : UserViewModel by lazy { ViewModelProvider(this)[UserViewModel::class.java] }
    private lateinit var binding : ActivityUserBinding
    private lateinit var id : UUID
    private lateinit var menu : Menu
    private lateinit var user : UserView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityUserBinding.inflate(layoutInflater)
        setContentView(binding.root)

        id = intent.getSerializableExtra("value") as UUID

        userViewModel.userViewList.observe(this, {userList -> userList.forEach { user ->
            if (user.id == id) {
                this.user = user
                updateUserView(user)
            }
        }
        })
    }

    private fun updateUserView(user: UserView) {
        with (binding) {
            image.setImageURI(user.imageName)
            name.text = user.name
            visitTime.text = user.lastVisit
            hobbyContent.text = user.hobby
            email.text = user.email
            statusContent.text = user.status
        }
    }

    override fun onOptionsItemSelected(item: MenuItem) = when (item.itemId) {
        R.id.home -> {
            NavUtils.navigateUpFromSameTask(this)
            true
        }
        R.id.action_save -> {
            actionSave()
            hideKeyboard()
            true
        }
        R.id.action_restore -> {
            actionRestore()
            true
        }
        R.id.action_edit -> {
            actionEdit()
            true
        }
        else -> {
            super.onOptionsItemSelected(item)
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.appbar_menu, menu)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        if (menu != null) {
            this.menu = menu
        }
        return super.onCreateOptionsMenu(menu)
    }

    private fun Context.hideKeyboard() {
        val inputMethodManager = getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        inputMethodManager.hideSoftInputFromWindow(binding.root.windowToken, 0)
    }

    private fun actionSave() {
        userViewModel.updateUser(binding.editName.text.toString(),
                                 binding.editEmail.text.toString(),
                                 binding.editHobbyContent.text.toString(),
                                 binding.editStatusContent.text.toString(),
                                 binding.spinner.selectedItem.toString(),
                                 user.id)

        binding.spinner.visibility = View.INVISIBLE
        binding.image.visibility = View.VISIBLE
        binding.editName.visibility = View.INVISIBLE
        binding.name.visibility = View.VISIBLE
        binding.editEmail.visibility = View.INVISIBLE
        binding.email.visibility = View.VISIBLE
        binding.editHobbyContent.visibility = View.INVISIBLE
        binding.hobbyContent.visibility = View.VISIBLE
        binding.editStatusContent.visibility = View.INVISIBLE
        binding.statusContent.visibility = View.VISIBLE

        menu.findItem(R.id.action_restore ).isVisible = false
        menu.findItem(R.id.action_save ).isVisible = false
        menu.findItem(R.id.action_edit ).isVisible = true
    }

    private fun actionRestore() {
        binding.image.setImageURI(user.imageName)
        binding.editName.setText(user.name)
        binding.editEmail.setText(user.email)
        binding.editHobbyContent.setText(user.hobby)
        binding.editStatusContent.setText(user.status)
    }

    private fun actionEdit() {
        binding.image.visibility = View.INVISIBLE
        binding.spinner.visibility = View.VISIBLE
        binding.editName.setText(user.name)
        binding.editName.visibility = View.VISIBLE
        binding.name.visibility = View.INVISIBLE
        binding.editEmail.setText(user.email)
        binding.editEmail.visibility = View.VISIBLE
        binding.email.visibility = View.INVISIBLE
        binding.editHobbyContent.setText(user.hobby)
        binding.editHobbyContent.visibility = View.VISIBLE
        binding.hobbyContent.visibility = View.INVISIBLE
        binding.editStatusContent.setText(user.status)
        binding.editStatusContent.visibility = View.VISIBLE
        binding.statusContent.visibility = View.INVISIBLE

        menu.findItem(R.id.action_restore ).isVisible = true
        menu.findItem(R.id.action_save ).isVisible = true
        menu.findItem(R.id.action_edit ).isVisible = false
    }
}