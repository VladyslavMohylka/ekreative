package com.example.hw_4.database

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Update
import com.example.hw_4.UserDatabase
import java.util.*

@Dao
interface UserDao {
    @Query("SELECT * FROM UserDatabase WHERE id=(:id)")
    fun getUser(id: UUID): LiveData<UserDatabase?>

    @Query("SELECT * FROM UserDatabase")
    fun getUsers(): LiveData<List<UserDatabase>>

    @Update
    fun updateUser(user: UserDatabase)

    @Insert
    fun addUser(user: UserDatabase)

    fun addStartUsers(users: List<UserDatabase>) {
        for (user in users) {
            addUser(user)
        }
    }
}