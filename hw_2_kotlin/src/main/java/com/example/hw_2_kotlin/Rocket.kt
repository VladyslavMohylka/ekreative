package com.example.hw_2_kotlin

import android.os.Parcel
import android.os.Parcelable
import android.os.Parcelable.Creator
import java.util.*


open class Rocket() : SpaceShip, Parcelable {
    private var cost: Int = 0
    private var weigh: Int = 0
    private var name: String? = null
    private var loadCapacity: Int = 0
    private var imageIndex: Int = 0
    private var chanceCrushLanding: Int = 0
    private var chanceCrushLaunching: Int = 0
    private var capacity = 0

    constructor(parcel: Parcel) : this() {
        name = parcel.readString()
        loadCapacity = parcel.readInt()
        chanceCrushLanding = parcel.readInt()
        chanceCrushLaunching = parcel.readInt()
        capacity = parcel.readInt()
    }

    constructor(
        cost: Int,
        weigh: Int,
        loadCapacity: Int,
        chanceCrushLanding: Int,
        chanceCrushLaunching: Int,
        imageIndex: Int,
        name: String?
    ) : this() {
        this.cost = cost
        this.weigh = weigh
        this.name = name
        this.loadCapacity = loadCapacity
        this.chanceCrushLanding = chanceCrushLanding
        this.chanceCrushLaunching = chanceCrushLaunching
        this.imageIndex = imageIndex
    }

    override fun carry(cargoWeigh: Int): Int {
        return if (canCarry(cargoWeigh)) {
            capacity += cargoWeigh
            chanceCrushLanding += cargoWeigh
            chanceCrushLaunching += cargoWeigh
            capacity
        } else {
            -1
        }
    }
    override fun canCarry(cargoWeigh: Int): Boolean {
        return capacity + cargoWeigh <= loadCapacity
    }
    override fun launch(): Boolean {
        return Random().nextInt(100) > chanceCrushLaunching
    }
    override fun land(): Boolean {
        return Random().nextInt(100) > chanceCrushLanding
    }
    @JvmName("getCost1")
    fun getCost() : Int {
        return cost
    }

    @JvmName("getLoadCapacity1")
    fun getLoadCapacity() : Int {
        return loadCapacity
    }
    @JvmName("getImageIndex1")
    fun getImageIndex(): Int {
        return imageIndex
    }
    @JvmName("getName1")
    fun getName(): String? {
        return name
    }

    override fun describeContents(): Int {
        return 0
    }

    override fun writeToParcel(p0: Parcel?, p1: Int) {
        p0?.writeInt(cost)
        p0?.writeInt(weigh)
        p0?.writeString(name)
        p0?.writeInt(loadCapacity)
        p0?.writeInt(imageIndex)
        p0?.writeInt(chanceCrushLanding)
        p0?.writeInt(chanceCrushLaunching)
        p0?.writeInt(capacity)
    }

    companion object CREATOR : Creator<Rocket> {
        override fun createFromParcel(parcel: Parcel): Rocket {
            return Rocket(parcel)
        }

        override fun newArray(size: Int): Array<Rocket?> {
            return arrayOfNulls(size)
        }
    }
}
