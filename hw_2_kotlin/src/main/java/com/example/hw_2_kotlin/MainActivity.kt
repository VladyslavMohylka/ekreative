package com.example.hw_2_kotlin

import android.content.Intent
import android.os.Bundle
import android.widget.Button
import android.widget.ImageButton
import androidx.appcompat.app.AppCompatActivity
import com.example.hw_2_kotlin.databinding.ActivityMainBinding
import java.util.*


class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding
    private lateinit var rockets: List<Rocket>
    private lateinit var activeRocket: Rocket

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        rockets = listOf(Rocket(100, 10, 18, 10, 20, 1, "U1"),
                         Rocket(120, 18, 29, 20, 10, 2, "U2"))
        activeRocket = rockets[0]

        addListenerToBtnNextRocket(binding.rightArrow)
        addListenerToBtnPrewRocket(binding.leftArrow)
        addListenerToBtnGoToSimulation(binding.goToSimulation)
    }
    private fun addListenerToBtnNextRocket(btn: Button) {
        btn.setOnClickListener {
            val isRocketExist: Boolean = activeRocket.let { rockets.indexOf(it) } > 0
            activeRocket = if (isRocketExist) rockets[(rockets.indexOf(activeRocket)) - 1] else rockets[rockets.size - 1]
            setRocketImage(activeRocket)
            binding.rocketName.text = activeRocket.getName()
        }
    }
    private fun addListenerToBtnPrewRocket(btn: ImageButton) {
        btn.setOnClickListener {
            val isRocketExist = rockets.indexOf(activeRocket) > 0
            activeRocket = if (isRocketExist) rockets[rockets.indexOf(activeRocket) - 1] else rockets[rockets.size - 1]
            setRocketImage(activeRocket)
            binding.rocketName.text = activeRocket.getName()
        }
    }
    private fun addListenerToBtnGoToSimulation(btn: Button) {
        btn.setOnClickListener {
            val intent = Intent(this@MainActivity, SimulationActivity::class.java)
            intent.putExtra(Rocket::class.java.canonicalName, activeRocket)
            startActivity(intent)
        }
    }
    private fun setRocketImage(activeRocket: Rocket) {
        val imageName = "@drawable/rocket_" + activeRocket.getImageIndex()
        binding.rocketImage.setImageResource(resources.getIdentifier(imageName, null, packageName))
    }
}