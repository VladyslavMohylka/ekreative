package com.example.hw_2_kotlin

interface SpaceShip {
    fun launch(): Boolean
    fun land(): Boolean
    fun canCarry(cargoWeigh: Int): Boolean
    fun carry(cargoWeigh: Int): Int
}
