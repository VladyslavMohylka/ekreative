package com.example.hw_2_kotlin

class Item(private var weigh: Int) {

    fun getWeigh(): Int {
        return weigh
    }

    fun setWeigh(weigh: Int) {
        this.weigh = weigh
    }
}