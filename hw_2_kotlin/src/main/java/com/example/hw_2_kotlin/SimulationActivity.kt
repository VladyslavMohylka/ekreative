package com.example.hw_2_kotlin

import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import com.example.hw_2_kotlin.databinding.ActivitySimulationBinding
import java.io.ByteArrayOutputStream
import java.io.IOException
import java.util.*

class SimulationActivity : AppCompatActivity() {
    private lateinit var binding: ActivitySimulationBinding
    private lateinit var rocket: Rocket
    private var cargo: List<Item>? = null
    private var rockets: List<Rocket?>? = null
    private var send = 0
    private var crushed = 0
    private var budgetValue = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivitySimulationBinding.inflate(layoutInflater)
        setContentView(binding.root)

        rocket = intent.getParcelableExtra(Rocket::class.java.canonicalName)!!
        binding.rocketName.text = rocket.getName()
        addListenerToBtnSimulate()
    }

    private fun addListenerToBtnSimulate() {
        binding.runSimulation.setOnClickListener {
            cargo = loadItems(1)
            rockets = loadRocket()

            for (ignored in rockets!!) {
                if (isCrush()) {
                    do {
                        send++
                        crushed++
                        budgetValue += rocket.getCost()
                    } while (isCrush())
                }
                send++
                budgetValue += rocket.getCost()
            }
            binding.totalSend.text = send.toString()
            binding.crushed.text = crushed.toString()
            binding.budget.text = budgetValue.toString()
        }
    }

    private fun loadItems(phaseNumber: Int): List<Item> {
        val result: MutableList<Item> = ArrayList<Item>()
        val fileName = "phase-$phaseNumber.txt"
        try {
            val `is` = assets.open(fileName)
            val baos = ByteArrayOutputStream()
            val buffer = ByteArray(1024)
            var length: Int
            while (`is`.read(buffer).also { length = it } != -1) {
                baos.write(buffer, 0, length)
            }
            val ss = baos.toString("UTF-8")
            val aa = ss.split("\n").toTypedArray()
            for (str in aa) {
                val zz = str.split("=").toTypedArray()
                result.add(Item(zz[1].toInt()))
            }
        } catch (e: IOException) {
            Log.e("TAG", "loadItems: $fileName file not found")
        }

        return result
    }

    private fun loadRocket(): List<Rocket?> {
        val result: MutableList<Rocket?> = ArrayList<Rocket?>()
        for (item in cargo!!) {
            while (item.getWeigh() > 0) {
                item.setWeigh(item.getWeigh() - rocket.getLoadCapacity())
                result.add(rocket)
            }
        }
        return result
    }

    private fun isCrush(): Boolean {
        return if (rocket.launch()) {
            rocket.land()
        } else {
            false
        }
    }
}


