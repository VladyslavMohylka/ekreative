package com.example.hw_1_java;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

public class MainActivity extends AppCompatActivity {
    private EditText editNumber;
    private ImageView lamp;
    private Button check;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        editNumber = findViewById(R.id.edit_number);
        lamp = findViewById(R.id.lamp);
        check = findViewById(R.id.check);

        addListenerToButton(check);
        addListenerToEditText();
    }
    private void addListenerToButton(Button btn) {
        btn.setOnClickListener(view -> {
            int number = Integer.parseInt(editNumber.getText().toString());
            int sum_1 = 0;
            int sum_2 = 0;

            for (int i = 0; i < 6; i++) {
                int ost = number % 10;
                number /= 10;
                if (i < 3) {
                    sum_1 += ost;
                } else {
                    sum_2 += ost;
                }
            }
            lamp.setImageResource((sum_1 == sum_2) ? R.drawable.green_lamp : R.drawable.red_lamp);
        });
    }
    private void addListenerToEditText() {
        editNumber.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                EditText edit = findViewById(R.id.edit_number);
                int number_length = edit.getText().toString().length();
                check.setEnabled(number_length == 6);
            }
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {}
            @Override
            public void afterTextChanged(Editable editable) {}
        });
    }
}