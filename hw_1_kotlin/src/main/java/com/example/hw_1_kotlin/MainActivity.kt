package com.example.hw_1_kotlin

import android.annotation.SuppressLint
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.widget.Button
import com.example.hw_1_kotlin.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        addListenerToButton(binding.check)
        addListenerToEditText()
    }

    private fun addListenerToEditText() {
        binding.editNumber.addTextChangedListener(object : TextWatcher {
            override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {
                val numberLength = binding.editNumber.text.toString().length
                binding.check.isEnabled = numberLength == 6
            }
            override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {}
            override fun afterTextChanged(editable: Editable) {}
        })
    }
    @SuppressLint("ResourceType", "UseCompatLoadingForDrawables")
    private fun addListenerToButton(btn : Button) {
        btn.setOnClickListener {
            val number = binding.editNumber.text.toString().toInt()
            val uri = if(isSumsEquals(number)) "@drawable/green_lamp" else "@drawable/red_lamp"
            val imageResource = resources.getIdentifier(uri, null, packageName)
            val res = resources.getDrawable(imageResource, null)
            binding.lamp.setImageDrawable(res)
        }
    }
    private fun isSumsEquals(number : Int) : Boolean {
        var sum1 = 0
        var sum2 = 0
        var numb = number

        for (i in 1..6) {
            val ost = numb % 10
            numb /= 10
            if (i < 4) {
                sum1 += ost
            } else {
                sum2 += ost
            }
        }

        return sum1 == sum2
    }
}