package com.example.hw_2_java;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.Random;


public class Rocket implements SpaceShip, Parcelable {
    private final int cost;
    private final int weigh;
    private final String name;
    private final int loadCapacity;
    private final int imageIndex;
    private int chanceCrushLanding;
    private int chanceCrushLaunching;
    private int capacity = 0;

    public Rocket(int cost, int weigh, int loadCapacity, int chanceCrushLanding, int chanceCrushLaunching, int imageIndex, String name) {
        this.cost = cost;
        this.weigh = weigh;
        this.name = name;
        this.loadCapacity = loadCapacity;
        this.chanceCrushLanding = chanceCrushLanding;
        this.chanceCrushLaunching = chanceCrushLaunching;
        this.imageIndex = imageIndex;
    }

    protected Rocket(Parcel in) {
        cost = in.readInt();
        weigh = in.readInt();
        name = in.readString();
        loadCapacity = in.readInt();
        imageIndex = in.readInt();
        chanceCrushLanding = in.readInt();
        chanceCrushLaunching = in.readInt();
        capacity = in.readInt();
    }
    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(cost);
        dest.writeInt(weigh);
        dest.writeString(name);
        dest.writeInt(loadCapacity);
        dest.writeInt(imageIndex);
        dest.writeInt(chanceCrushLanding);
        dest.writeInt(chanceCrushLaunching);
        dest.writeInt(capacity);
    }
    @Override
    public int describeContents() {
        return 0;
    }
    public static final Creator<Rocket> CREATOR = new Creator<Rocket>() {
        @Override
        public Rocket createFromParcel(Parcel in) {
            return new Rocket(in);
        }

        @Override
        public Rocket[] newArray(int size) {
            return new Rocket[size];
        }
    };
    @Override
    public int carry(int cargoWeigh) {
        if(canCarry(cargoWeigh)) {
            this.capacity += cargoWeigh;
            this.chanceCrushLanding += cargoWeigh;
            this.chanceCrushLaunching += cargoWeigh;
            return this.capacity;
        } else {
            return -1;
        }
    }
    @Override
    public boolean canCarry(int cargoWeigh) {
        return this.capacity + cargoWeigh <= loadCapacity;
    }
    @Override
    public boolean launch() {
        return new Random().nextInt(100) > chanceCrushLaunching;
    }
    @Override
    public boolean land() {
        return new Random().nextInt(100) > chanceCrushLanding;
    }
    public int getImageIndex() {
        return imageIndex;
    }
    public String getName() {
        return name;
    }
    public int getCost() {
        return cost;
    }
    public int getLoadCapacity() {
        return loadCapacity;
    }

    public int unCarry(int cargoWeigh) {
        if(this.capacity - cargoWeigh >= 0) {
            this.capacity -= cargoWeigh;
            this.chanceCrushLanding -= cargoWeigh;
            this.chanceCrushLaunching -= cargoWeigh;
            return this.capacity;
        } else {
            return -1;
        }
    }

}
