package com.example.hw_2_java;

public class Item {
    private int weigh;

    public Item(int weigh) {
        this.weigh = weigh;
    }

    public int getWeigh() {
        return weigh;
    }

    public void setWeigh(int weigh) {
      this.weigh = weigh;
    }
}
