package com.example.hw_2_java;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    private TextView rocketName;
    private ImageView rocketImage;
    private Button prevRocket;
    private Button nextRocket;
    private Button goToSimulation;
    List<Rocket> rockets;
    Rocket activeRocket;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        rocketName = findViewById(R.id.rocketName);
        rocketImage = findViewById(R.id.rocketImage);
        prevRocket = findViewById(R.id.leftArrow);
        nextRocket = findViewById(R.id.rightArrow);
        goToSimulation = findViewById(R.id.goToSimulation);

        rockets = new ArrayList<>();
        rockets.add(new Rocket(100, 10, 18, 10, 20, 1, "U1"));
        rockets.add(new Rocket(120, 18, 29, 20, 10, 2, "U2"));
        activeRocket = rockets.get(0);

        addListenerToBtnNextRocket(nextRocket);
        addListenerToBtnPrewRocket(prevRocket);
        addListenerToBtnGoToSimulation(goToSimulation);
    }
    private void addListenerToBtnNextRocket(Button btn) {
        btn.setOnClickListener(view -> {
            boolean isRocketExist = rockets.indexOf(activeRocket) + 1 < rockets.size();
            activeRocket = isRocketExist ? rockets.get(rockets.indexOf(activeRocket) + 1) : rockets.get(0);
            setRocketImage(activeRocket);
            rocketName.setText(activeRocket.getName());
        });
    }
    private void addListenerToBtnPrewRocket(Button btn) {
        btn.setOnClickListener(view -> {
            boolean isRocketExist = rockets.indexOf(activeRocket) > 0;
            activeRocket = isRocketExist ? rockets.get(rockets.indexOf(activeRocket) - 1) : rockets.get(rockets.size() - 1);
            setRocketImage(activeRocket);
            rocketName.setText(activeRocket.getName());
        });
    }
    private void addListenerToBtnGoToSimulation(Button btn) {
        btn.setOnClickListener(view -> {
            Intent intent = new Intent(MainActivity.this, SimulationActivity.class);
            intent.putExtra(Rocket.class.getCanonicalName(), activeRocket);
            startActivity(intent);
        });
    }
    private void setRocketImage(Rocket rocket) {
        String imageName = "@drawable/rocket_" + rocket.getImageIndex();
        rocketImage.setImageResource(getResources().getIdentifier(imageName, null, getPackageName()));
    }
}

