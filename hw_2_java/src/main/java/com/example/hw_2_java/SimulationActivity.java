package com.example.hw_2_java;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.TextView;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

public class SimulationActivity extends AppCompatActivity {
    TextView totalSend;
    TextView totalCrushed;
    TextView rocketName;
    TextView budget;
    Button simulate;
    Rocket rocket;
    List<Item> cargo;
    List<Rocket> rockets;
    int send = 0;
    int crushed = 0;
    int budgetValue = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_simulation);
        totalSend = findViewById(R.id.total_send);
        totalCrushed = findViewById(R.id.crushed);
        rocketName = findViewById(R.id.rocket_name);
        simulate = findViewById(R.id.run_simulation);
        budget = findViewById(R.id.budget);
        rocket =  getIntent().getParcelableExtra(Rocket.class.getCanonicalName());
        rocketName.setText(rocket.getName());
        addListenerToBtnSimulate();
    }

    private void addListenerToBtnSimulate() {
        simulate.setOnClickListener(view -> {
            cargo = loadItems(1);
            rockets = loadRocket();
            Log.i("TAG", "addListenerToBtnSimulate: " + rockets.size());
            for(Rocket ignored : rockets) {
                if (isCrush()) {
                    do {
                        send++;
                        crushed++;
                        budgetValue += rocket.getCost();
                    } while (isCrush());
                }
                send++;
                budgetValue += rocket.getCost();
            }
            totalSend.setText(Integer.toString(send));
            totalCrushed.setText(Integer.toString(crushed));
            budget.setText(Integer.toString(budgetValue));
        });
    }
    private List<Item> loadItems(int phaseNumber) {
        List<Item> result = new ArrayList<>();
        String fileName = "phase-" + phaseNumber + ".txt";
        try {
            InputStream is = getAssets().open(fileName);
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            byte[] buffer = new byte[1024];
            for (int length; (length = is.read(buffer)) != -1; ) {
                baos.write(buffer, 0, length);
            }
             String ss = baos.toString("UTF-8");
            String[] aa = ss.split("\n");
            for(String str : aa) {
                String[] zz = str.split("=");
                result.add(new Item(Integer.parseInt(zz[1])));
            }
        } catch (IOException e) {
            Log.e("TAG", "loadItems: " + fileName + " file not found");
        }

        return result;
    }

    private List<Rocket> loadRocket() {
        List<Rocket> result = new ArrayList<>();

        for(Item item : cargo) {
            while(item.getWeigh() > 0) {
                item.setWeigh(item.getWeigh() - rocket.getLoadCapacity());
                result.add(rocket);
            }
        }

        return result;
    }

    private boolean isCrush() {
        if(rocket.launch()) {
            return rocket.land();
        } else {
            return false;
        }
    }
}
