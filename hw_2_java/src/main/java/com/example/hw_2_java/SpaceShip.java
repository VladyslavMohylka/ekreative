package com.example.hw_2_java;

public interface SpaceShip {
    boolean launch();
    boolean land();
    boolean canCarry(int cargoWeigh);
    int carry(int cargoWeigh);
}
