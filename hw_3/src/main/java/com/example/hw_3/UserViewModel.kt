package com.example.hw_3

import android.net.Uri
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import java.util.*

class UserViewModel : ViewModel() {
    private val userMutableList = MutableLiveData<List<User>>()
    val userList: LiveData<List<User>> = userMutableList

    init {
        userMutableList.value = UsersData().getUsersList()
    }

    private fun getUser(id : Int) : User {
        return userList.value?.find { it.id == id }!!
    }
    fun getIdList() : List<Int> {
        return userList.value!!.map {it.id}
    }
    fun getImageUri(id : Int) : Uri {
       return Uri.parse("android.resource://" + BuildConfig.APPLICATION_ID + "/drawable/" + getUser(id).imageName)
     }
    fun getName(id : Int) : String {
        return getUser(id).name
    }
    fun getVisitTime(id : Int) : String {
        val lastVisitInMinutes = (Calendar.getInstance().timeInMillis - getUser(id).lastVisit)/60000
        val result : String = when (lastVisitInMinutes) {
            in 0..5 -> "5 minutes"
            in 5..60 -> "hour"
            in 60..3600 -> "day"
            in 3600..86400 -> "month"
            else -> "year"
        }

        return "Last visit during $result"
    }
    fun getHobby(id : Int) : String {
        return getUser(id).hobby
    }
    fun getEmail(id : Int) : String {
        return getUser(id).email
    }
    fun getStatus(id : Int) : String {
        return getUser(id).status
    }


}