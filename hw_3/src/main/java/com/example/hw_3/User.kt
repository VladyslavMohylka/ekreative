package com.example.hw_3

data class User(
    val name: String, var imageName: String, var lastVisit: Long,
    val hobby: String, val email: String, val status: String, val id: Int)