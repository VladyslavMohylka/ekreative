package com.example.hw_3

import android.os.Bundle
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import com.example.hw_3.databinding.ActivityUserBinding

class UserActivity : AppCompatActivity() {
    private val userViewModel : UserViewModel by lazy { ViewModelProvider(this).get(UserViewModel::class.java)}
    private lateinit var binding : ActivityUserBinding
    private var id : Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityUserBinding.inflate(layoutInflater)
        setContentView(binding.root)
        id = intent.getIntExtra("id", 0)

        updateUserView(id)
    }

    private fun updateUserView(id: Int) {
        val imageView : ImageView =  findViewById(R.id.image)
        imageView.setImageURI(userViewModel.getImageUri(id))
        val textViewName : TextView =  findViewById(R.id.name)
        textViewName.text = userViewModel.getName(id)
        val textViewVisitTime : TextView =  findViewById(R.id.visit_time)
        textViewVisitTime.text = userViewModel.getVisitTime(id)
        val textViewHobby : TextView =  findViewById(R.id.hobby_content)
        textViewHobby.text = userViewModel.getHobby(id)
        val textViewEmail : TextView =  findViewById(R.id.email)
        textViewEmail.text = userViewModel.getEmail(id)
        val textViewStatus : TextView =  findViewById(R.id.status_content)
        textViewStatus.text = userViewModel.getStatus(id)
    }
}