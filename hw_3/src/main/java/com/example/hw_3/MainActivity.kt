package com.example.hw_3

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.example.hw_3.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {
    private val userViewModel : UserViewModel by lazy { ViewModelProvider(this).get(UserViewModel::class.java)}
    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val userObserver = Observer<List<User>> {
            updateUserListView()
        }
        userViewModel.userList.observe(this, userObserver)
    }

    private fun updateUserListView() {
        val userIdList = userViewModel.getIdList()
        for (id in userIdList) {
            addUserView(id)
        }
    }
    private fun addUserView(id: Int) {
        val containerView : View = LayoutInflater.from(this).inflate(R.layout.user_list_view, null)
        containerView.tag = id
        val imageView : ImageView =  containerView.findViewById(R.id.image)
        imageView.setImageURI(userViewModel.getImageUri(id))
        val textViewName : TextView =  containerView.findViewById(R.id.name)
        textViewName.text = userViewModel.getName(id)
        val textViewVisitTime : TextView =  containerView.findViewById(R.id.visit_time)
        textViewVisitTime.text = userViewModel.getVisitTime(id)

        containerView.setOnClickListener {
            val intent = Intent(this@MainActivity, UserActivity::class.java)
            intent.putExtra("id", containerView.tag as Int)
            startActivity(intent)
        }
        binding.usersListContainer.addView(containerView, binding.usersListContainer.childCount)
    }
}

