package com.example.hw_3

import java.util.*
import kotlin.random.Random
import kotlin.random.nextLong
class UsersData {
   private var usersList : List<User> = listOf(
       User("John", "id_1", getRandomTime(),"sport","www.john1.com","sleep",1000 ),
       User("Tomas", "id_2", getRandomTime(),"sport, films","www.john2.com","active",1001 ),
       User("Nikolas", "id_3", getRandomTime(),"sport, travel","www.john3.com","smile",1002 ),
       User("Peter", "id_4", getRandomTime(),"sport, films, travel","www.john4.com","other",1003 ),
       User("Albert", "id_5", getRandomTime(),"sport, cars","www.john5.com","sleep",1004 ),
       User("Rick", "id_6", getRandomTime(),"sport, chess","www.john6.com","sleep",1005 ),
       User("Donald", "id_1", getRandomTime(),"sport, chess","www.john7.com","sleep",1006 ),
       User("Bred", "id_4", getRandomTime(),"sport, chess","www.john8.com","sleep",1007 )
   )

    fun getUsersList(): List<User> {
        return usersList
    }

  private fun getRandomTime(): Long {
      val calendar = Calendar.getInstance()
      calendar.set(2021, 9,23,0,0,0)

      return Random.nextLong(calendar.timeInMillis..Calendar.getInstance().timeInMillis)
   }
}





