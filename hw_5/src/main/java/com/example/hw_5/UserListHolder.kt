package com.example.hw_5

import android.content.Context
import android.view.View
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import de.hdodenhof.circleimageview.CircleImageView

class UserListHolder(val item: View) : RecyclerView.ViewHolder(item) {
    var imageView: CircleImageView? = null
    var textViewName: TextView? = null
    var textViewVisitTime: TextView? = null
    var parent: RelativeLayout? = null
    var tag: Any? = null
    var context: Context

    init {
        imageView = item.findViewById(R.id.image)
        textViewName = item.findViewById(R.id.name)
        textViewVisitTime = item.findViewById(R.id.visit_time)
        parent = item.findViewById(R.id.user_list_view)
        tag = item.tag
        context = item.context
    }
}