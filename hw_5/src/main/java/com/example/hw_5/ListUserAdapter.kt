package com.example.hw_5

import android.content.Intent
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.ContextCompat.startActivity
import androidx.recyclerview.widget.RecyclerView
import com.example.hw_5.activity.user_details.UserActivity
import com.example.hw_5.activity.user_details.UserActivityState
import java.util.*

class ListUserAdapter(data: List<UserDataView>) : RecyclerView.Adapter<UserListHolder>() {
    private var data = listOf<UserDataView>()
    init {
        this.data = data
    }

    override fun onBindViewHolder(holder: UserListHolder, position: Int) {
        holder.imageView?.setImageURI(data[position].imageName)
        holder.textViewName?.text = data[position].name
        holder.textViewVisitTime?.text = data[position].lastVisit
        holder.tag = data[position].id

        holder.parent?.setOnClickListener{
            val intent = Intent(holder.context, UserActivity::class.java)
                intent.putExtra("value", holder.tag as UUID)
                intent.putExtra("state", UserActivityState.SHOW)
                startActivity(holder.context,intent,null)
        }
    }

    override fun getItemCount(): Int {
        return data.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): UserListHolder {
        val itemView =
            LayoutInflater.from(parent.context)
                .inflate(R.layout.user_list_view, parent, false)

        return UserListHolder(itemView)
    }
}