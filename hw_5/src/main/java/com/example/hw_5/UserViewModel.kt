package com.example.hw_5

import android.net.Uri
import androidx.lifecycle.*
import com.example.hw_5.database.UserDataDatabase
import com.example.hw_5.database.UserRepository
import java.lang.System.currentTimeMillis
import java.util.*

class UserViewModel : ViewModel() {
    private var userRepository = UserRepository.get()
    private val _userViewList = MutableLiveData<List<UserDataView>>()
    var userViewList: LiveData<List<UserDataView>> = _userViewList
    private val _userDatabaseList = MutableLiveData<List<UserDataDatabase>>()
    var userDataDatabaseList: LiveData<List<UserDataDatabase>> = _userDatabaseList

    init {
        userDataDatabaseList = liveData {
            val data = userRepository.getUsers()
            emitSource(data)
        }
        userViewList = Transformations.switchMap(userDataDatabaseList) { list -> convertData(list)}
    }

    private fun convertData(data: List<UserDataDatabase>): LiveData<List<UserDataView>> {
        val _userList = MutableLiveData<List<UserDataView>>()
        val userList: LiveData<List<UserDataView>> = _userList
        val result = mutableListOf<UserDataView>()

        for (user in data) {
            result.add(
                UserDataView(
                    user.name, getImageUri(user.imageName), getVisitTime(user.lastVisit),
                    user.hobby, user.email, user.status, user.id
                )
            )
        }
        _userList.postValue(result)

        return userList
    }

    fun getImageUri(name : String) : Uri {
        return Uri.parse("android.resource://com.example.hw_5//drawable/$name")
    }

    private fun getVisitTime(milliseconds : Long) : String {
        val result : String = when ((Calendar.getInstance().timeInMillis - milliseconds)/60000) {
            in 0..5 -> "5 minutes"
            in 5..60 -> "hour"
            in 60..3600 -> "day"
            in 3600..86400 -> "month"
            else -> "year"
        }

        return "Last visit during $result"
    }

    fun updateUser(name: String, email: String, hobby: String, status: String, imageName: String, id: UUID) {
        val userDataDatabase = UserDataDatabase(name, imageName, currentTimeMillis(), hobby, email, status, id)
        if (isUserExist(id)) {
            userRepository.updateUser(userDataDatabase)
        } else {
            userRepository.addUser(userDataDatabase)
        }
    }

    fun getUser(id : UUID) : UserDataView {
        return userViewList.value?.find { user -> user.id == id}!!
    }

    private fun isUserExist(id : UUID) : Boolean {
        return userViewList.value?.find { user -> user.id == id} != null
    }

    fun createLocalUser() : UserDataView {
        return UserDataView(
            "", Uri.parse("android.resource://com.example.hw_5//drawable/1"), "",
            "", "", "", UUID.randomUUID()
        )
    }

    fun removeUser(id : UUID) {
        userRepository.removeUser(id)
    }

    fun getImageNames(): List<String> {
      return  listOf(
          "id_1",
          "id_2",
          "id_3",
          "id_4",
          "id_5",
          "id_6",
          "id_7",
          "id_8",
      )
    }
}