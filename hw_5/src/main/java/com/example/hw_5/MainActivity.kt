package com.example.hw_5

import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.hw_5.activity.user_details.UserActivity
import com.example.hw_5.activity.user_details.UserActivityState
import com.example.hw_5.databinding.ActivityMainBinding
import java.util.*

class MainActivity : AppCompatActivity() {
    private val userViewModel : UserViewModel by lazy { ViewModelProvider(this).get(UserViewModel::class.java)}
    private lateinit var binding: ActivityMainBinding
    private lateinit var  recyclerView: RecyclerView
    private lateinit var menu : Menu

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        userViewModel.userViewList.observe(this, {userList -> updateUserListView(userList)})

        recyclerView = binding.recyclerView
        recyclerView.layoutManager = LinearLayoutManager(this)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.main_appbar_menu, menu)
        if (menu != null) {
            this.menu = menu
        }
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem) = when (item.itemId) {
        R.id.action_create -> {
            val intent = Intent(this@MainActivity, UserActivity::class.java)
            intent.putExtra("state", UserActivityState.CREATE)
            startActivity(intent)
            true
        }
        else -> {
            false
        }
    }

    private fun updateUserListView(userList: List<UserDataView>) {
        recyclerView.adapter = ListUserAdapter(userList)
    }
}

