package com.example.hw_5

import android.net.Uri
import androidx.room.PrimaryKey
import java.util.*

data class UserDataView(var name: String, var imageName: Uri, var lastVisit: String,
                        var hobby: String, var email: String, var status: String, @PrimaryKey val id: UUID = UUID.randomUUID())
