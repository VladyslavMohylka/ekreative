package com.example.hw_5.database

import android.app.Application
import com.example.hw_5.database.UserRepository

class DatabaseApplication : Application() {
    override fun onCreate() {
        super.onCreate()
        UserRepository.initialize(this)
    }
}