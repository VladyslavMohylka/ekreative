package com.example.hw_5.database

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.sqlite.db.SupportSQLiteDatabase
import com.example.hw_5.database.UserDataDatabase
import com.example.hw_5.database.UserDatabase
import kotlinx.coroutines.*
import java.lang.IllegalStateException
import java.util.*
import java.util.concurrent.Executors
import kotlin.random.Random
import kotlin.random.nextLong


private const val DATABASE_NAME = "user_database"

class UserRepository private constructor(context: Context) {
    private val testData = listOf(
        UserDataDatabase("John", "id_1", getRandomTime(), "sport", "www.john1.com", "sleep", UUID.randomUUID()),
        UserDataDatabase("Nikolas", "id_3", getRandomTime(), "sport, travel", "www.john3.com", "smile", UUID.randomUUID()),
        UserDataDatabase("Peter", "id_4", getRandomTime(), "sport, films, travel", "www.john4.com", "other", UUID.randomUUID()),
        UserDataDatabase("Albert", "id_5", getRandomTime(), "sport, cars", "www.john5.com", "sleep", UUID.randomUUID()),
        UserDataDatabase("Rick", "id_6", getRandomTime(), "sport, chess", "www.john6.com", "sleep", UUID.randomUUID()),
        UserDataDatabase("Donald", "id_1", getRandomTime(), "sport, chess", "www.john7.com", "sleep", UUID.randomUUID()),
        UserDataDatabase("Bred", "id_4", getRandomTime(), "sport, chess", "www.john8.com", "sleep", UUID.randomUUID())
    )
    private val database: UserDatabase = Room.databaseBuilder(
        context.applicationContext,
        UserDatabase::class.java,
        DATABASE_NAME
    ).addCallback(object : RoomDatabase.Callback() {
        override fun onCreate(db: SupportSQLiteDatabase) {
            super.onCreate(db)
            CoroutineScope(Dispatchers.Default).launch {
                get().userDao.addStartUsers(testData)
            }
        }
    }).build()
    private val userDao = database.userDao()
    private val executor = Executors.newSingleThreadExecutor()

    suspend fun updateItems() = withContext(Dispatchers.IO + NonCancellable) {
        get().userDao.addStartUsers(testData)
    }

    fun getUser(id: UUID): LiveData<UserDataDatabase?> = userDao.getUser(id)

    fun getUsers(): LiveData<List<UserDataDatabase>> = userDao.getUsers()

    fun updateUser(userData: UserDataDatabase) {
        executor.execute {
            userDao.updateUser(userData)
        }
    }

    fun addUser(userData: UserDataDatabase) {
        executor.execute {
            userDao.addUser(userData)
        }
    }

    fun removeUser(id: UUID) {
        executor.execute {
            userDao.deleteUser(id)
        }
    }

    private fun getRandomTime(): Long {
        val calendar = Calendar.getInstance()
        calendar.set(2021, 10, 23, 0, 0, 0)

        return Random.nextLong(calendar.timeInMillis..Calendar.getInstance().timeInMillis)
    }

    companion object {
        private var INSTANCE: UserRepository? = null

        fun initialize(context: Context) {
            if (INSTANCE == null) {
                INSTANCE = UserRepository(context)
            }
        }

        fun get(): UserRepository {
            return INSTANCE ?: throw IllegalStateException("UserRepository must be  initialized")
        }
    }
}
