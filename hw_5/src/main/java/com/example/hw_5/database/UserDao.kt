package com.example.hw_5.database

import androidx.lifecycle.LiveData
import androidx.room.*
import java.util.*

@Dao
interface UserDao {
    @Query("SELECT * FROM UserDataDatabase WHERE id=(:id)")
    fun getUser(id: UUID): LiveData<UserDataDatabase?>

    @Query("SELECT * FROM UserDataDatabase")
    fun getUsers(): LiveData<List<UserDataDatabase>>

    @Update
    fun updateUser(userData: UserDataDatabase)

    @Insert
    fun addUser(userData: UserDataDatabase)

    @Query("DELETE FROM UserDataDatabase WHERE id=(:id)")
    fun deleteUser(id: UUID)

    fun addStartUsers(userData: List<UserDataDatabase>) {
        for (user in userData) {
            addUser(user)
        }
    }
}