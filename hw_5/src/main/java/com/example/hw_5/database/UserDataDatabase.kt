package com.example.hw_5.database

import androidx.room.Entity
import androidx.room.PrimaryKey
import java.util.*

@Entity
data class UserDataDatabase(
        var name: String, var imageName: String, var lastVisit: Long,
        var hobby: String, var email: String, var status: String, @PrimaryKey val id: UUID = UUID.randomUUID())