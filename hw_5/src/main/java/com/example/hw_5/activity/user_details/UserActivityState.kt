package com.example.hw_5.activity.user_details

enum class UserActivityState {
    CREATE,
    EDIT,
    SHOW
}