package com.example.hw_5.activity.user_details

import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import com.example.hw_5.R


class UserImageRecyclerAdapter(private val names: List<String>, private val onItemClicked: (position: String) -> Unit) : RecyclerView
.Adapter<UserImageRecyclerAdapter.MyViewHolder>() {
    class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val parent: ConstraintLayout? = itemView.findViewById(R.id.recyclerview_parent)
        val image: ImageView? = itemView.findViewById(R.id.recyclerview_image)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val itemView = LayoutInflater.from(parent.context)
            .inflate(R.layout.activity_user_recyclerview_image, parent, false)
        return MyViewHolder(itemView)
    }
    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val name = names[position]
        holder.image?.setImageURI(Uri.parse("android.resource://com.example.hw_5//drawable/$name"))
        holder.image?.tag = name
        holder.parent?.setOnClickListener{
            onItemClicked(holder.image?.tag.toString())
        }
    }
    override fun getItemCount() = names.size
}

