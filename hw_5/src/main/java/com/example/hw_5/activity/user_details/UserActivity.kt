package com.example.hw_5.activity.user_details

import android.app.Activity
import android.content.Context
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.NavUtils
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.hw_5.R
import com.example.hw_5.UserViewModel
import com.example.hw_5.UserDataView
import com.example.hw_5.databinding.ActivityUserBinding
import com.google.android.material.navigation.NavigationBarView
import java.util.*


class UserActivity : AppCompatActivity() {
    private val userViewModel : UserViewModel by lazy { ViewModelProvider(this)[UserViewModel::class.java] }
    private val observer = {
        user = userViewModel.getUser(id)
        updateUserView(user)
    }
    private lateinit var binding : ActivityUserBinding
    private lateinit var id : UUID
    private lateinit var menu : Menu
    private lateinit var user : UserDataView
    private lateinit var state : UserActivityState
    private lateinit var bottomNavigation : NavigationBarView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityUserBinding.inflate(layoutInflater)
        setContentView(binding.root)
        bottomNavigation = binding.userListNavigation
        state = intent.getSerializableExtra("state") as UserActivityState

        addBottomNavigationListener()
        start(state)
    }

    private fun onListItemClick(position: String) {
        Toast.makeText(this,position, Toast.LENGTH_SHORT).show()
        binding.image.tag = position
        binding.image.setImageURI(userViewModel.getImageUri(position))
    }

    private fun start(state : UserActivityState) {
        when(state) {
            UserActivityState.CREATE -> {
                user = userViewModel.createLocalUser()
                actionEdit()
            }
            UserActivityState.EDIT -> {
            }
            UserActivityState.SHOW -> {
                val intentId = intent.getSerializableExtra("value")
                id = intentId as UUID
                observe()
            }
        }
    }

    private fun  observe() {
        userViewModel.userViewList.observe(this, {
            observer()
        })
    }

    private fun addBottomNavigationListener() {
        bottomNavigation.setOnItemSelectedListener { item ->
            when (item.itemId) {
                R.id.ic_add -> {
                    return@setOnItemSelectedListener  true
                }
                R.id.ic_send_message -> {
                    return@setOnItemSelectedListener  true
                }
                R.id.ic_remove -> {
                    userViewModel.userViewList.removeObservers(this)
                    NavUtils.navigateUpFromSameTask(this)
                    userViewModel.removeUser(id)

                    return@setOnItemSelectedListener  true
                }
            }
            false
        }
    }

    private fun updateUserView(user: UserDataView) {
        with (binding) {
            image.setImageURI(user.imageName)
            name.text = user.name
            visitTime.text = user.lastVisit
            hobbyContent.text = user.hobby
            email.text = user.email
            statusContent.text = user.status
        }
    }

    override fun onOptionsItemSelected(item: MenuItem) = when (item.itemId) {
        R.id.home -> {
            NavUtils.navigateUpFromSameTask(this)
            true
        }
        R.id.action_save -> {
            state =  UserActivityState.SHOW
            invalidateOptionsMenu()
            actionSave()
            hideKeyboard()
            true
        }
        R.id.action_restore -> {
            actionRestore()
            true
        }
        R.id.action_edit -> {
            state =  UserActivityState.EDIT
            invalidateOptionsMenu()
            actionEdit()
            true
        }
        else -> {
            super.onOptionsItemSelected(item)
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.appbar_menu, menu)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        if (menu != null) {
            this.menu = menu
        }
        when(state) {
            UserActivityState.CREATE,
            UserActivityState.EDIT -> {
                this.menu.findItem(R.id.action_restore).isVisible = true
                this.menu.findItem(R.id.action_save).isVisible = true
                this.menu.findItem(R.id.action_edit).isVisible = false
            }
            UserActivityState.SHOW -> {
                this.menu.findItem(R.id.action_restore).isVisible = false
                this.menu.findItem(R.id.action_save).isVisible = false
                this.menu.findItem(R.id.action_edit).isVisible = true
            }
        }

        return true
    }

    private fun Context.hideKeyboard() {
        val inputMethodManager = getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        inputMethodManager.hideSoftInputFromWindow(binding.root.windowToken, 0)
    }

    private fun actionSave() {
        userViewModel.updateUser(binding.editName.text.toString(),
                                 binding.editEmail.text.toString(),
                                 binding.editHobbyContent.text.toString(),
                                 binding.editStatusContent.text.toString(),
                                 binding.image.tag.toString(),
                                 user.id)
        binding.recyclerView.visibility = View.INVISIBLE
        binding.image.visibility = View.VISIBLE
        binding.editName.visibility = View.INVISIBLE
        binding.name.visibility = View.VISIBLE
        binding.editEmail.visibility = View.INVISIBLE
        binding.email.visibility = View.VISIBLE
        binding.editHobbyContent.visibility = View.INVISIBLE
        binding.hobbyContent.visibility = View.VISIBLE
        binding.editStatusContent.visibility = View.INVISIBLE
        binding.statusContent.visibility = View.VISIBLE
    }

    private fun actionRestore() {
        binding.image.setImageURI(user.imageName)
        binding.editName.setText(user.name)
        binding.editEmail.setText(user.email)
        binding.editHobbyContent.setText(user.hobby)
        binding.editStatusContent.setText(user.status)
    }

    private fun actionEdit() {
        val recyclerView: RecyclerView = binding.recyclerView
        recyclerView.layoutManager = LinearLayoutManager(this)
        recyclerView.adapter = UserImageRecyclerAdapter(userViewModel.getImageNames()
        ) { position -> onListItemClick(position) }

        binding.image.visibility = View.INVISIBLE
        binding.recyclerView.visibility = View.VISIBLE
        binding.editName.setText(user.name)
        binding.editName.visibility = View.VISIBLE
        binding.name.visibility = View.INVISIBLE
        binding.editEmail.setText(user.email)
        binding.editEmail.visibility = View.VISIBLE
        binding.email.visibility = View.INVISIBLE
        binding.editHobbyContent.setText(user.hobby)
        binding.editHobbyContent.visibility = View.VISIBLE
        binding.hobbyContent.visibility = View.INVISIBLE
        binding.editStatusContent.setText(user.status)
        binding.editStatusContent.visibility = View.VISIBLE
        binding.statusContent.visibility = View.INVISIBLE
    }
}